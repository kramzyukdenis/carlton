﻿using card;
using state;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace game
{
    class GameStepState : BaseState
    {
        private ICardStack deck;
        private ICardStack[] finalStacks;
        private ICardStack[] tmpStacks;

        private Action transition;

        private IEnumerable<ICard> cardsDragged;
        private Vector3 dragOffset;
        private Vector3 prevMousePosition;
        private Plane plane = new Plane(Vector3.up, Vector3.zero);

        private ICardStack activeStack;
        private bool needTransition = false;

        public GameStepState(
                ICardStack deck,
                ICardStack[] finalStacks,
                ICardStack[] tmpStacks
            )
        {
            this.deck = deck;
            this.finalStacks = finalStacks;
            this.tmpStacks = tmpStacks;
        }

        public void SetTransition(Action action) => transition = action;

        public override void Enter()
        {
            needTransition = false; 

            foreach (var stack in tmpStacks.Concat(finalStacks))
            {
                Action<IEnumerable<ICard>> dragStart = (cards) =>
                {
                    activeStack = stack;
                    HandleDragStart(cards);
                };
                stack.DragStart += dragStart;
                stack.DragEnd += HandleDragEnd;
            }

            deck.Clicked += HandleDeckClick;
        }

        public override void Exit()
        {
            foreach (var stack in tmpStacks)
            {
                stack.DragStart = null;
                stack.DragEnd -= HandleDragEnd;
            }

            deck.Clicked -= HandleDeckClick;
        }

        public override void Update()
        {
            if (cardsDragged != null)
            {
                var currentPosition = GetPositionOnPlane();
                var delta = currentPosition - prevMousePosition;
                prevMousePosition = currentPosition;
                dragOffset += delta;

                foreach (var card in cardsDragged)
                {
                    card.Position += delta;
                }
            }

            if (needTransition) transition();
        }

        private void HandleDragStart(IEnumerable<ICard> cards)
        {
            cardsDragged = cards;
            prevMousePosition = GetPositionOnPlane();
            dragOffset = Vector3.up * 0.01f;

            foreach (var card in cards)
            {
                card.Position += dragOffset;
                card.Collider.enabled = false;
            }
        }

        private void HandleDragEnd()
        {
            var stackWithType = GetHoveredStack();
            var stack = stackWithType.Item1;
            var isFinalStack = stackWithType.Item2;

            if (stack != activeStack && stack != null)
            {
                if (isFinalStack)
                {
                    if (CanDropFinal(stack))
                    {
                        HandleDrop(stack);
                        foreach (var card in cardsDragged)
                        {
                            card.Collider.enabled = true;
                        }
                        cardsDragged = null;

                        if (IsGameEnd()) needTransition = true;
                        return;
                    }
                }
                else
                {
                    if (CanDropTmp(stack))
                    {
                        HandleDrop(stack);
                        foreach (var card in cardsDragged)
                        {
                            card.Collider.enabled = true;
                        }
                        cardsDragged = null;
                        return;
                    }
                }
            }

            foreach (var card in cardsDragged)
            {
                card.Position -= dragOffset;
                card.Collider.enabled = true;
            }

            cardsDragged = null;
        }


        private Vector3 GetPositionOnPlane()
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            float distance;
            if (plane.Raycast(ray, out distance))
            {
                return ray.GetPoint(distance);
            }

            return Vector3.zero;
        }

        private (ICardStack, bool) GetHoveredStack()
        {
            foreach (var stack in tmpStacks)
            {
                if (stack.Hovered) return (stack, false);
            }

            foreach (var stack in finalStacks)
            {
                if (stack.Hovered) return (stack, true);
            }

            return (null, false);
        }

        private bool CanDropTmp(ICardStack cardStack)
        {
            var card = cardsDragged.First();

            if (cardStack.Count() == 0)
            {
                if (card.Value == CardValue.King) return true;

                return false;
            }

            var cardLast = cardStack.Last();

            if ((int)card.Type % 2 != (int)cardLast.Type % 2 &&
                card.Value - cardLast.Value == -1)
                return true;

            return false;
        }

        private bool CanDropFinal(ICardStack cardStack)
        {
            if (cardsDragged.Count() > 1) return false;

            var card = cardsDragged.First();

            if (cardStack.Count() == 0)
            {
                if (card.Value == CardValue.Ace) return true;

                return false;
            }

            var cardLast = cardStack.Last();

            if (card.Type == cardLast.Type && card.Value - cardLast.Value == 1)
                return true;

            return false;
        }

        private void HandleDrop(ICardStack stackTo)
        {
            activeStack.RemoveFrom(cardsDragged.First());
            stackTo.Add(cardsDragged);
        }

        private void HandleDeckClick()
        {
            Debug.Log("clicked");
            foreach (var stack in tmpStacks)
            {
                var card = deck.Last();
                deck.Remove(card);
                stack.Add(card);

                if (deck.Count() == 0) break;
            }
        }

        private bool IsGameEnd()
        {
            return finalStacks.All((stack) => stack.Count() == 13);
        }
    }
}
