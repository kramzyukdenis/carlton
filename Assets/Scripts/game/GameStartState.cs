﻿using card;
using state;
using System;
using System.Collections.Generic;
using System.Linq;

namespace game
{
    class GameStartState : BaseState
    {
        private ICardStack deck;
        private ICardStack[] tmpStacks;

        private ICardFactory cardFactory;

        private Action transition;

        public GameStartState(
                ICardFactory cardFactory,
                ICardStack deck,
                ICardStack[] finalStacks,
                ICardStack[] tmpStacks
            )
        {
            this.deck = deck;
            this.tmpStacks = tmpStacks;
            this.cardFactory = cardFactory;
        }

        public void SetTransition(Action action) => transition = action;

        public override void Enter()
        {
            var cards = new List<ICard>(52);

            for (int type = 0; type < 4; type++)
            {
                for (int value = 0; value < 13; value++)
                {
                    cards.Add(
                        cardFactory.CreateCard(
                            (CardType)type,
                            (CardValue)value
                            )
                        );
                }
            }

            var rnd = new Random();
            cards = cards.OrderBy(item => rnd.Next()).ToList();

            for (int i = 0; i < tmpStacks.Length; i++)
            {
                for (int j = 0; j <= i; j++)
                {
                    var card = cards.Last();
                    cards.Remove(card);
                    tmpStacks[i].Add(card);
                }
            }

            deck.Add(cards);
        }

        public override void Update()
        {
            transition();
        }
    }
}
