﻿using card;
using state;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace game
{
    class GameController : MonoBehaviour
    {
        private IFiniteStateMachine fsm;

        private ICardStack deck;
        private ICardStack[] finalStacks;
        private ICardStack[] tmpStacks;

        [SerializeField] private CardFactory factory;

        [SerializeField] private GameObject deckTarget;
        [SerializeField] private GameObject[] finalStacksTargets;
        [SerializeField] private GameObject[] tmpStacksTargets;

        [SerializeField] private GameObject panel;
        [SerializeField] private Button buttonRestart;

        private void Start()
        {
            fsm = new FiniteStateMachine();

            deck = factory.CreateFinalCardStack(deckTarget);

            finalStacks = new CardStack[4];
            tmpStacks = new CardStack[4];

            for (int i = 0; i < 4; i++)
            {
                finalStacks[i] = factory.CreateFinalCardStack(
                    finalStacksTargets[i]);
                tmpStacks[i] = factory.CreateTMPCardStack(
                    tmpStacksTargets[i]);
            }

            buttonRestart.onClick.AddListener(HandleClick);

            Func<IState, string, IState> logInOutState =
                (IState state, string name) =>
            {
                return new LogShell(state, name);
            };

            var startGameState = new GameStartState(
                factory,
                deck,
                finalStacks,
                tmpStacks
            );

            var finalStartGameState = logInOutState(
                startGameState,
                "Start Game"
            );

            var stepState = new GameStepState(
                deck,
                finalStacks,
                tmpStacks
            );

            var finalStepState = logInOutState(
                stepState,
                "Step"
            );

            var gameEndState = new GameEndState(panel);

            var finalGameEndState = logInOutState(
                stepState,
                "Game End"
            );

            startGameState.SetTransition(() => fsm.Active = finalStepState);
            stepState.SetTransition(() => fsm.Active = finalGameEndState);

            fsm.Active = finalStartGameState;
        }

        private void Update()
        {
            fsm.Update();
        }

        private void LateUpdate()
        {
            fsm.LateUpdate();
        }

        private void FixedUpdate()
        {
            fsm.FixedUpdate();
        }

        private void HandleClick()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
