﻿using state;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace game
{
    class GameEndState : BaseState
    {
        private GameObject panel;

        public GameEndState(GameObject panel)
        {
            this.panel = panel;
        }

        public override void Enter()
        {
            panel.SetActive(true);
        }       
    }
}
