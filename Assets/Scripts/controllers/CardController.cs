﻿using card;
using UnityEngine;

namespace controllers
{
    class CardController : MonoBehaviour
    {
        private ICard card;
        
        public void Set(ICard card)
        {
            this.card = card;
            
            card.Moved += Move;
        }

        private void Move()
        {
            transform.position = card.Position;
        }

        private void OnMouseDown()
        {
            card.DragStart?.Invoke();
        }

        private void OnMouseUp()
        {
            card.DragEnd?.Invoke();
        }

        private void OnMouseEnter()
        {
            card.Hovered = true;
        }

        private void OnMouseExit()
        {
            card.Hovered = false;
        }

        private void OnMouseUpAsButton()
        {
            Debug.Log("card clicked");
            card.Clicked?.Invoke();
        }
    }
}
