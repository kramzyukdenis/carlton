﻿using card;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace controllers
{
    class CardStackController : MonoBehaviour
    {
        private ICardStack cardStack;

        private Vector3 cardOffset;

        private bool dragStarted = false;
        private Dictionary<ICard, Action> startDragHandlers =
            new Dictionary<ICard, Action>();

        public void Set(ICardStack cardStack, Vector3 cardOffset)
        {
            this.cardStack = cardStack;
            this.cardOffset = cardOffset;

            cardStack.Added += AddHandler;
            cardStack.Removed += RemoveHandler;
        }

        private void AddHandler(ICard card)
        {
            card.Position =
                transform.position +
                cardOffset * cardStack.GetIndex(card) +
                new Vector3(0f, cardOffset.y, 0f);

            Action handleDragStart = () =>
            {
                var subStack = cardStack.SubStack(card);
                if (!CanDrag(subStack)) return;

                cardStack.DragStart?.Invoke(subStack);
                dragStarted = true;
            };

            startDragHandlers.Add(card, handleDragStart);

            card.DragStart += handleDragStart;
            card.DragEnd += HandleDragEnd;
        }

        private void RemoveHandler(ICard card)
        {
            var action = startDragHandlers[card];
            startDragHandlers.Remove(card);
            card.DragStart -= action;
            card.DragEnd -= HandleDragEnd;
        }

        private void HandleDragEnd()
        {
            if (dragStarted)
            {
                cardStack.DragEnd?.Invoke();
                dragStarted = false;
            }
        }

        private bool CanDrag(IEnumerable<ICard> cards)
        {
            var list = new List<ICard>(cards);
            for (int i = 0; i < list.Count - 1; i++)
            {
                if (list[i].Value - list[i + 1].Value != 1 ||
                    (int)list[i].Type % 2 == (int)list[i + 1].Type % 2)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
