﻿using UnityEngine;

namespace state
{
    class LogShell : IState
    {
        private IState state;
        private string name;

        private const string colorName = "#0000ff";
        private const string colorEnter = "#0a8f30";
        private const string colorExit = "#ff0000";

        public LogShell(IState state, string name)
        {
            this.state = state;
            this.name = name;
        }

        public void Enter()
        {
            Debug.Log(
                    string.Format(
                        "<color={0}>Enter </color><color={1}>{2}</color>",
                        colorEnter, 
                        colorName,
                        name)
                );

            state.Enter();
        }

        public void Exit()
        {
            Debug.Log(
                   string.Format(
                        "<color={0}>Exit </color><color={1}>{2}</color>",
                        colorExit,
                        colorName,
                        name)
               );

            state.Exit();
        }

        public void FixedUpdate()
        {
            state.FixedUpdate();
        }

        public void LateUpdate()
        {
            state.LateUpdate();
        }

        public void Update()
        {
            state.Update();
        }
    }
}
