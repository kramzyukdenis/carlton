﻿namespace state
{
    class FiniteStateMachine : IFiniteStateMachine
    {
        private IState active;

        public IState Active
        {
            get { return active; }
            set
            {
                active?.Exit();
                active = value;
                active?.Enter();
            }
        }

        public void Enter()
        {
            active?.Enter();
        }

        public void Exit()
        {
            active?.Exit();
        }

        public void FixedUpdate()
        {
            active?.FixedUpdate();
        }

        public void LateUpdate()
        {
            active?.LateUpdate();
        }

        public void Update()
        {
            active?.Update();
        }
    }
}