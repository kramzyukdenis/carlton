﻿namespace state
{
	public interface IFiniteStateMachine : IState
	{
		IState Active { get; set; }
	}
}