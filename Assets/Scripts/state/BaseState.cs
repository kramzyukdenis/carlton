﻿namespace state
{
	class BaseState : IState
	{
		public virtual void Enter() { }
		public virtual void Exit() { }
		public virtual void FixedUpdate() { }
		public virtual void LateUpdate() { }
		public virtual void Update() { }
    }
}
