﻿namespace common
{
    interface IHoverable
    {
        bool Hovered { get; set; }
    }
}
