﻿using System;

namespace common
{
    interface IClickable
    {
        Action Clicked { get; set; }
    }
}
