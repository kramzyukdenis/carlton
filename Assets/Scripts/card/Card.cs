﻿using System;
using UnityEngine;

namespace card
{
    class Card : ICard
    {
        public CardType Type { get; }
        public CardValue Value { get; }

        private Vector3 position;
        public Vector3 Position
        {
            get => position;
            set
            {
                position = value;
                Moved?.Invoke();
            }
        }

        public Action Moved { get; set; }
        public Action DragStart { get; set; }
        public Action DragEnd { get; set; }
        public bool Hovered { get; set; }
        public Action Clicked { get; set; }

        public Collider Collider { get; }

        public Card() { }

        public Card(CardType type, CardValue value, Collider collider)
        {
            Type = type;
            Value = value;
            Collider = collider;
        }
    }
}
