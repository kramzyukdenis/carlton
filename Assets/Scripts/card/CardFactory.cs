﻿using controllers;
using UnityEngine;

namespace card
{
    class CardFactory : MonoBehaviour, ICardFactory
    {
        [SerializeField] private GameObject[] cardPrefabs;

        public ICard CreateCard(CardType type, CardValue value)
        {

            var obj = Instantiate(cardPrefabs[(int)type * 13 + (int)value]);
            var collider = obj.AddComponent<BoxCollider>();

            var card = new Card(type, value, collider);

            obj.AddComponent<CardController>().Set(card);
            obj.transform.localScale = Vector3.one * 0.05f;

            return card;
        }

        private ICardStack CreateCardStack(Vector3 position, Vector3 cardOffset,
            GameObject blankCard)
        {
            var blank = new Card();
            blankCard.AddComponent<CardController>().Set(blank);

            var cardStack = new CardStack(blank);

            var obj = new GameObject("CardStack");
            obj.transform.position = position;
            obj.AddComponent<CardStackController>()
                .Set(cardStack, cardOffset);

            return cardStack;
        }

        public ICardStack CreateFinalCardStack(GameObject blankCard)
        {
            return CreateCardStack(blankCard.transform.position,
                new Vector3(0f, 0.0001f, 0f), blankCard);
        }

        public ICardStack CreateTMPCardStack(GameObject blankCard)
        {
            return CreateCardStack(blankCard.transform.position, 
                new Vector3(0f, 0.0001f, -0.08f), blankCard);
        }
    }
}
