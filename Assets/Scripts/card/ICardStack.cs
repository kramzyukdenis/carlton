﻿using common;
using System;
using System.Collections.Generic;

namespace card
{
    interface ICardStack : IHoverable, IEnumerable<ICard>, IClickable
    {
        void Add(ICard card);
        void Add(IEnumerable<ICard> cards);

        void Remove(ICard card);
        void RemoveFrom(ICard card);

        int GetIndex(ICard card);
        IEnumerable<ICard> SubStack(ICard cardFrom);

        Action<ICard> Added { get; set; }
        Action<ICard> Removed { get; set; }
        Action<IEnumerable<ICard>> DragStart { get; set; }
        Action DragEnd { get; set; }
    }
}
