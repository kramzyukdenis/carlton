﻿using UnityEngine;

namespace card
{
    interface ICardFactory
    {
        ICard CreateCard(CardType type, CardValue value);
        ICardStack CreateFinalCardStack(GameObject blankCard);
        ICardStack CreateTMPCardStack(GameObject blankCard);
    }
}
