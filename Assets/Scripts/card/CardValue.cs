﻿namespace card
{
    enum CardValue
    {
        Ace = 0,
        V2,
        V3,
        V4,
        V5,
        V6,
        V7,
        V8,
        V9,
        V10,
        Jack,
        Queen,
        King
    }
}
