﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace card
{
    class CardStack : ICardStack
    {
        private List<ICard> cards = new List<ICard>();

        public Action<ICard> Added { get; set; }
        public Action<ICard> Removed { get; set; }
        public Action<IEnumerable<ICard>> DragStart { get; set; }
        public Action DragEnd { get; set; }

        private ICard blankCard;

        public CardStack(ICard blankCard)
        {
            this.blankCard = blankCard;
        }

        public bool Hovered
        {
            get => cards.Any((card) => card.Hovered) || blankCard.Hovered;
            set { }
        }

        public Action Clicked { get; set; }

        public void Add(ICard card)
        {
            cards.Add(card);
            Added?.Invoke(card);

            card.Clicked += HandleCardClick;
        }

        public void Add(IEnumerable<ICard> cards)
        {
            foreach (var card in cards)
            {
                Add(card);
            }
        }

        public IEnumerator<ICard> GetEnumerator()
        {
            return cards.GetEnumerator();
        }

        public int GetIndex(ICard card)
        {
            return cards.IndexOf(card);
        }

        public void Remove(ICard card)
        {
            if (cards.Remove(card))
            {
                Removed?.Invoke(card);

                card.Clicked -= HandleCardClick;
            }
        }

        public void RemoveFrom(ICard card)
        {
            var index = cards.IndexOf(card);

            for (int i = cards.Count - 1; i >= index; i--)
            {
                Remove(cards[i]);
            }
        }

        public IEnumerable<ICard> SubStack(ICard cardFrom)
        {
            var subStack = new List<ICard>();
            var index = cards.IndexOf(cardFrom);

            for (int i = index; i < cards.Count; i++)
            {
                subStack.Add(cards[i]);
            }

            return subStack;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private void HandleCardClick()
        {
            Debug.Log("stack clicked");
            Clicked?.Invoke();
        }
    }
}
