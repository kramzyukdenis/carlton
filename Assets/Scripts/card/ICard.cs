﻿using common;
using System;
using UnityEngine;

namespace card
{
    interface ICard : IHoverable, IClickable
    {
        CardType Type { get; }
        CardValue Value { get; }
        Vector3 Position { get; set; }

        Action Moved { get; set; }
        Action DragStart { get; set; }
        Action DragEnd { get; set; }

        Collider Collider { get; }
    }
}